#!/usr/bin/env python
import ply.lex

def strip_comments(source):
    tokens = (
                'PERCENT', 'BEGINCOMMENT', 'ENDCOMMENT', 'BACKSLASH',
                'CHAR', 'BEGINVERBATIM', 'ENDVERBATIM', 'NEWLINE', 'ESCPCT',
             )
    states = (
                ('linecomment', 'exclusive'), 
                ('commentenv', 'exclusive'), 
                ('verbatim', 'exclusive')
            )
    
    #Deal with escaped backslashes, so we don't think they're escaping %.
    def t_ANY_BACKSLASH(t):
        r"\\\\"
        return t
        
    #One-line comments
    def t_PERCENT(t):
        r"\%"
        t.lexer.begin("linecomment")
     
    #Escaped percent signs
    def t_ESCPCT(t):
        r"\\\%"
        return t
    
    #Comment environment, as defined by verbatim package       
    def t_BEGINCOMMENT(t):
        r"\\begin\s*{\s*comment\s*}"
        t.lexer.begin("commentenv")
    
    #Verbatim environment (different treatment of comments within)   
    def t_BEGINVERBATIM(t):
        r"\\begin\s*{\s*verbatim\s*}"
        t.lexer.begin("verbatim")
        return t
    
    #Any other character in initial state we leave alone    
    def t_CHAR(t):
        r"."
        return t
        
    def t_NEWLINE(t):
        r"\n"
        return t
    
    #End comment environment    
    def t_commentenv_ENDCOMMENT(t):
        r"\\end\s*{\s*comment\s*}"
        #Anything after \end{comment} on a line is ignored!
        t.lexer.begin('linecomment')
    
    #Ignore comments of comment environment    
    def t_commentenv_CHAR(t):
        r"."
        pass
        
    def t_commentenv_NEWLINE(t):
        r"\n"
        pass
    
    #End of verbatim environment    
    def t_verbatim_ENDVERBATIM(t):
        r"\\end\s*{\s*verbatim\s*}"
        t.lexer.begin('INITIAL')
        return t
        
    #Leave contents of verbatim environment alone
    def t_verbatim_CHAR(t):
        r"."
        return t
        
    def t_verbatim_NEWLINE(t):
        r"\n"
        return t
    
    #End a % comment when we get to a new line
    def t_linecomment_ENDCOMMENT(t):
        r"\n"
        t.lexer.begin("INITIAL")
        #Newline at the end of a line comment is stripped.
    
    #Ignore anything after a % on a line        
    def t_linecomment_CHAR(t):
        r"."
        pass
        
    lexer = ply.lex.lex(errorlog=ply.lex.NullLogger())
    lexer.input(source)
    return u"".join([tok.value for tok in lexer])

import os
import re

from subprocess import Popen, PIPE, STDOUT
import subprocess
from PIL import Image

image_path = ['images']
image_ext = ['pdf', 'eps', 'png', 'jpg', 'jpeg']

# The list of files to process.
files = [
    'python_intro.tex',
    'ann.tex',
    'fuzzy.tex',
    'rapidminer.tex',
    'cp.tex',
    'search.tex'
]

# Define the regular expressions.
student_note = re.compile('\\\StudentSticky{.*}{[^}]*}', re.M)
several_newlines = re.compile('\n[\s\n]+', re.M)
verbatimrefs = re.compile('\\\\verbatiminput\{(.*)\}')
coderefs = re.compile('\\\inputcode(?:\[([^\]]*)\])?\{(?:.*)\}\{(.*)\}')
figures = re.compile('\\\includegraphics(?:\[([^\]]*)\])?\{([^\}]*)\}')
re_option = re.compile('([a-z]*)=([0-9.]*)([a-z]*)')
re_identify = re.compile(" ([0-9.,]+)x([0-9.,]+) ")

# Call detex on the specified text.
def detex(text):
    return subprocess.check_output(["detex"], input=str.encode(text)).decode()

def image_size(filename):
    out = subprocess.check_output(["identify", filename], input=str.encode(text)).decode()
    out = re_identify.search(out)
    
    if(out == None): raise ValueError('Could not extract the size from identify output.') 
    
    try:
        return float(out.group(1)), float(out.group(2))
    except ValueError:
        return None

def get_filename(name):
    for path in image_path:
        for ext in image_ext:
            filename = path + os.path.sep + name + '.' + ext
            if os.path.isfile(filename): return filename
    
# Compute image area given the latex options and the image filename. 
def compute_image_area(options, image):
    options = re_option.findall(options)
    
    for opt in options:
        opt_width = None
        opt_height = None        
        
        if opt[0] == 'width':
            if opt[2] != 'cm': return None
            
            try:
                opt_width = float(opt[1])                
            except ValueError:
                return None
                
        elif opt[0] == 'height':
            if opt[2] != 'cm': return None
                
            try:
                opt_height = float(opt[1])
            except ValueError:
                return None
        else:
            raise RuntimeError("Unknown option '{}'.".format(opt[0]))
            
            
        if(opt_width != None and opt_height != None):
            return opt_width*opt_height
    
        filename = get_filename(image)
        if filename == None: return None
    
        im_width, im_height = image_size(filename)
        ratio = im_width/im_height
        
        if opt_width != None:
            height = opt_width/ratio
            return opt_width*height
            
        elif opt_height != None:            
            width = opt_height*ratio
            return width*opt_height
    
    return None

fulltext = ''
codefiles = []
verbatimfiles = []
images = []

# Text.
for file in files:
    text = open(file).read()
   
    # Strip comments before doing anything else.
    text = strip_comments(text)
    # Remove student notes.
    text = student_note.sub('', text)
    # List referenced code files.
    codefiles.extend(coderefs.findall(text))
    # Verbatiminput files.
    verbatimfiles.extend(verbatimrefs.findall(text))
    # List images.    
    images.extend(figures.findall(text))
    # Add to total text.
    fulltext += text
    
fulltext = detex(fulltext)
fulltext = several_newlines.sub('\n', fulltext)
count = len(fulltext)

# Code listings.
codecount = 0

# We also want to count verbatiminput.
codefiles.extend([('', f) for f in verbatimfiles])

for optstr, file in codefiles:
    options = re_option.findall(optstr)
    firstline = None
    lastline = None
    
    for opt in options:
        if opt[0] == "firstline":
            firstline = int(opt[1])
        elif opt[0] == "lastline":
            lastline = int(opt[1])
               
    if firstline is None and lastline is None:
        codecount += len(open(file).read())
    elif firstline is None:
        codecount += len("".join(open(file).readlines()[:lastline]))
    elif lastline is None:
        codecount += len("".join(open(file).readlines()[firstline-1:]))
    else:
        codecount += len("".join(open(file).readlines()[firstline-1:lastline]))

totaltext = count + codecount

print("Text má dokopy {} znakov. Z toho {} tvorí text a {} zdrojový kód.".format(
        totaltext, count, codecount))
print("Počet AH je {:.2f}. Z toho {:.2f} tvorí text a {:.2f} zdrojový kód.".format(
        totaltext/36000, count/36000, codecount/36000))

uncounted_images = []       
# Image areas.
image_area = 0
for image in images:
    area = compute_image_area(image[0], image[1])    
    if(area != None):
        image_area += area
    else:
        uncounted_images.append(image)

print("Obsah obrázkov je {} cm2, čo tvorí celkovo {:.2f} AH.".format(image_area, image_area/2300))

if(len(uncounted_images) > 0):
    print("Nemožno automaticky určiť obsah nasledujúcich obrázkov:\n{}".format(
          "\n".join([str(ii[1]) for ii in uncounted_images])
          ))

print("Osobitne treba zrátať AH za bibliografiu a za slovník!!!")
