#!/bin/sh
libreoffice -convert-to eps *.svm

for EPSFILE in *.eps; do 
    ps2eps ${EPSFILE}
	rm ${EPSFILE}
	mv "${EPSFILE}.eps" ${EPSFILE}
done
