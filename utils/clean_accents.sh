#!/bin/sh

#small letters

sed -i "s/\\\'a/á/g" $1
sed -i "s/\\\'e/é/g" $1
sed -i "s/\\\'i/í/g" $1
sed -i "s/\\\'o/ó/g" $1
sed -i "s/\\\'u/ú/g" $1
sed -i "s/\\\'y/ý/g" $1
sed -i "s/\\\'l/ĺ/g" $1

sed -i "s/\\\v{c}/č/g" $1
sed -i "s/\\\v{e}/ě/g" $1
sed -i "s/\\\v{d}/ď/g" $1
sed -i "s/\\\v{l}/ľ/g" $1
sed -i "s/\\\v{n}/ň/g" $1
sed -i "s/\\\v{r}/ř/g" $1
sed -i "s/\\\v{s}/š/g" $1
sed -i "s/\\\v{t}/ť/g" $1
sed -i "s/\\\v{z}/ž/g" $1

sed -i "s/\\\^o/ô/g" $1
sed -i "s/\\\\\"a/ä/g" $1

#capital letters

sed -i "s/\\\'A/Á/g" $1
sed -i "s/\\\'E/É/g" $1
sed -i "s/\\\'I/Í/g" $1
sed -i "s/\\\'O/Ó/g" $1
sed -i "s/\\\'U/Ú/g" $1
sed -i "s/\\\'Y/Ý/g" $1
sed -i "s/\\\'L/Ĺ/g" $1

sed -i "s/\\\v{C}/Č/g" $1
sed -i "s/\\\v{E}/Ě/g" $1
sed -i "s/\\\v{D}/Ď/g" $1
sed -i "s/\\\v{L}/Ľ/g" $1
sed -i "s/\\\v{N}/Ň/g" $1
sed -i "s/\\\v{R}/Ř/g" $1
sed -i "s/\\\v{S}/Š/g" $1
sed -i "s/\\\v{T}/Ť/g" $1
sed -i "s/\\\v{Z}/Ž/g" $1

sed -i "s/\\\^O/Ô/g" $1
sed -i "s/\\\\\"A/Ä/g" $1
