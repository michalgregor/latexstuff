[General]
IniMode=true

[version]
written_by_TXS_version=2.9.4
written_by_TXS_hg_revision=:
created_by_TXS_version=2.9.4
created_by_TXS_hg_revision=:

[texmaker]
Tools\Commands\latex="latex -output-directory auxfiles -interaction=nonstopmode %.tex"
Tools\Commands\pdflatex="pdflatex -synctex=1 -output-directory auxfiles -shell-escape -interaction=nonstopmode \"\\def\\auxfiles{}\\input{%.tex}\""
Tools\Commands\xelatex="xelatex -synctex=1 -output-directory auxfiles -interaction=nonstopmode %.tex"
Tools\Commands\lualatex="lualatex -synctex=1 -output-directory auxfiles -interaction=nonstopmode %.tex"
Tools\Commands\view-dvi=evince auxfiles/%.dvi
Tools\Commands\view-ps=evince auxfiles/%.ps
Tools\Commands\view-pdf-external=evince auxfiles/%.pdf
Tools\Commands\dvips=dvips -o %.ps %.dvi
Tools\Commands\dvipng=dvipng -T tight -D 120 %.dvi
Tools\Commands\ps2pdf=ps2pdf %.ps
Tools\Commands\dvipdf=dvipdfm %.dvi
Tools\Commands\bibtex=bibtex %.aux
Tools\Commands\bibtex8=bibtex8 %.aux
Tools\Commands\biber=biber %
Tools\Commands\makeindex=makeindex %.idx
Tools\Commands\texindy=
Tools\Commands\makeglossaries=makeglossaries %
Tools\Commands\metapost="mpost --interaction nonstopmode "
Tools\Commands\asy=asy ?m*.asy
Tools\Commands\gs=gs
Tools\Commands\latexmk="latexmk -silent -bibtex -outdir=auxfiles -pdf -pdflatex=\"pdflatex -synctex=1 --shell-escape %%O '\\def\\auxfiles{}\\input{%%S}'\" %"
Tools\Commands\quick=txs:///compile | txs:///view
Tools\Commands\compile=txs:///latexmk
Tools\Commands\view=txs:///view-pdf
Tools\Commands\view-pdf=txs:///view-pdf-internal
Tools\Commands\bibliography=txs:///bibtex
Tools\Commands\index=txs:///makeindex
Tools\Commands\glossary=txs:///makeglossaries
Tools\Commands\ps-chain=txs:///latex | txs:///dvips | txs:///view-ps
Tools\Commands\dvi-chain=txs:///latex | txs:///view-dvi
Tools\Commands\pdf-chain=txs:///pdflatex | txs:///view-pdf
Tools\Commands\dvi-pdf-chain=txs:///latex | txs:///dvipdf | txs:///view-pdf
Tools\Commands\dvi-ps-pdf-chain=txs:///latex | txs:///dvips | txs:///ps2pdf | txs:///view-pdf
Tools\Commands\asy-dvi-chain=txs:///latex | txs:///asy | txs:///latex | txs:///view-dvi
Tools\Commands\asy-pdf-chain=txs:///pdflatex | txs:///asy | txs:///pdflatex | txs:///view-pdf
Tools\Commands\pre-compile=
Tools\Commands\internal-pre-compile=txs:///pre-compile | txs:///conditionally-recompile-bibliography
Tools\Commands\recompile-bibliography=txs:///compile | txs:///bibliography | txs:///compile
Tools\Commands\svn="svn "
Tools\Commands\svnadmin="svnadmin "
Tools\Commands\userold1=txs:///latex | txs:///dvips | txs:///ps2pdf | txs:///view-pdf
Tools\Commands\user0="pdftex -ini -jobname=\"%\" \"&pdflatex\" mylatexformat.ltx %.tex"
Tools\Commands\makeglossary=makeglossaries %
ToolBar\CentralVisible=true
StructureView\ShowLinenumbers=false
StructureView\Indentation=-1
StructureView\IndentIncludes=false
Structure\ShowElementsInComments=false
Structure\MarkStructureElementsBeyondEnd=true
Structure\MarkStructureElementsInAppendix=true
StructureView\ReferenceCommandsInContextMenu=\\ref
Files\New%20File%20Encoding=UTF-8
Files\AutoDetectEncodingFromChars=true
Files\AutoDetectEncodingFromLatex=true
Common%20Encodings=UTF-8, ISO-8859-1, windows-1252, Apple Roman
Files\Max%20Recent%20Files=5
Files\Max%20Recent%20Projects=3
Files\Max%20Recent%20Sessions=5
Files\Recent%20Files=/home/michalgregor/Desktop/ucebnica/ucebnica.tex, /home/michalgregor/Desktop/ucebnica/evo.tex, /home/michalgregor/Desktop/ucebnica/modules/abbterms.tex, /home/michalgregor/texmf/bibtex/bib/bibliography.bib, /home/michalgregor/Documents/UI/ucebnica/ann_unsupervised.tex
Files\Recent%20Project%20Files=@Invalid()
Files\Recent%20Session%20Files=@Invalid()
Files\Remember%20File%20Filter=true
Files\RestoreSession=false
Files\Last%20Document=/home/michalgregor/Desktop/ucebnica/ucebnica.tex
Files\Parse%20BibTeX=true
Bibliography\BibFileEncoding=UTF-8
Files\Parse%20Master=true
Files\Autosave=0
Files\Autoload=false
Files\Bib%20Paths=/home/michalgregor/texmf/bibtex/bib
Files\Image%20Paths=
Editor\UseEscForClosingFullscreen=false
Editor\GoToErrorWhenDisplayingLog=true
Editor\ShowLogMarkersWhenClickingLogEntry=true
Editor\ScanInstalledLatexPackages=true
Tools\Insert%20Unicode%20From%20SymbolGrid=false
Spell\DictionaryDir=/usr/share/myspell/dicts
Spell\Language=en_GB
Spell\Dic=/usr/share/myspell/dicts/en_GB.dic
Thesaurus\Database=/usr/share/mythes/th_en_US_v2.dat
Update\AutoCheck=true
Update\AutoCheckInvervalDays=7
Update\LastCheck=@Variant(\0\0\0\x10\0%~X\x1\x65h\xc2\x1)
Editor\WordWrapMode=1
Editor\WrapLineWidth=80
Editor\Parentheses%20Matching=true
Editor\Parentheses%20Completion=true
Editor\Line%20Number%20Multiples=1
Editor\Cursor%20Surrounding%20Lines=5
Editor\BoldCursor=true
Editor\Auto%20Indent=true
Editor\Weak%20Indent=true
Editor\Indent%20with%20Spaces=false
Editor\Folding=true
Editor\Show%20Line%20State=true
Editor\Show%20Cursor%20State=true
Editor\Real-Time%20Spellchecking=true
Editor\Check%20Spelling=true
Editor\Check%20Citations=true
Editor\Check%20References=true
Editor\Check%20Syntax=true
Editor\Check%20Grammar=true
Editor\Check%20Package=true
Editor\Check%20In%20Non%20TeX%20Files=true
Editor\Hide%20Spelling%20Errors%20in%20Non%20Text=true
Editor\Hide%20Grammar%20Errors%20in%20Non%20Text=true
Editor\Show%20Whitespace=false
Editor\TabStop=4
Editor\ToolTip%20Help=true
Editor\ToolTip%20Preview=true
Editor\MaxImageTooltipWidth=400
Editor\ContextMenuKeyboardModifiers=33554432
Editor\ContextMenuSpellcheckingEntryLocation=0
Editor\TexDoc%20Help%20Internal=true
Editor\SilentReload=false
Editor\Replace%20Quotes=6
Editor\Close%20Search%20Replace%20Together=false
Editor\Use%20Line%20For%20Search=true
Editor\Search%20Only%20In%20Selection=true
Editor\Auto%20Replace%20Commands=true
Editor\Font%20Family=DejaVu Sans Mono
Editor\Font%20Size=11
Editor\Line%20Spacing%20Percent=100
Editor\Esc%20for%20closing%20log=false
Editor\Mouse%20Wheel%20Zoom=true
Editor\Smooth%20Scrolling=true
Editor\Hack%20Auto%20Choose=true
Editor\Hack%20Disable%20Fixed%20Pitch=false
Editor\Hack%20Disable%20Width%20Cache=false
Editor\Hack%20Disable%20Line%20Cache=false
Editor\Hack%20Disable%20Accent%20Workaround=false
Editor\Hack%20Render%20Mode=0
Editor\Completion=true
Editor\Completion%20Case%20Sensitive=2
Editor\Completion%20Complete%20Common%20Prefix=true
Editor\Completion%20EOW%20Completes=true
Editor\Completion%20Enable%20Tooltip%20Help=true
Editor\Completion%20Use%20Placeholders=true
Editor\Completion%20Show%20Placeholders=true
Editor\Completion%20Prefered%20Tab=0
Editor\Completion%20Tab%20Relative%20Font%20Size%20Percent=100
Editor\Auto%20Insert%20LRM=false
Editor\Visual%20Column%20Mode=true
Editor\Overwrite%20Opening%20Bracket%20Followed%20By%20Placeholder=true
Editor\Overwrite%20Closing%20Bracket%20Following%20Placeholder=true
TableAutoformat\Special%20Commands="\\hline,\\cline,\\intertext,\\shortintertext,\\toprule,\\midrule,\\bottomrule"
TableAutoformat\Special%20Command%20Position=0
TableAutoformat\One%20Line%20Per%20Cell=false
Grammar\Long%20Repetition%20Check=true
Grammar\Bad%20Word%20Check=true
Grammar\Long%20Repetition%20Check%20Distance=3
Grammar\Very%20Long%20Repetition%20Check%20Distance=10
Grammar\Very%20Long%20Repetition%20Check%20Min%20Length=6
Grammar\Word%20Lists%20Dir=/usr/share/texstudio
Grammar\Language%20Tool%20URL=http://localhost:8081/
Grammar\Language%20Tool%20Path=
Grammar\Language%20Tool%20Java%20Path=java
Grammar\Language%20Tool%20Autorun=false
Grammar\Language%20Tool%20Ignored%20Rules=
Grammar\Special%20Rules1=
Grammar\Special%20Rules2=
Grammar\Special%20Rules3=
Grammar\Special%20Rules4=
Dialogs\Last%20Hard%20Wrap%20Column=80
Dialogs\Last%20Hard%20Wrap%20Smart%20Scope%20Selection=false
Dialogs\Last%20Hard%20Wrap%20Join%20Lines=false
Tools\SingleViewerInstance=false
Tools\Show%20Messages%20When%20Compiling=true
Tools\Show%20Stdout=1
Tools\Automatic%20Rerun%20Times=5
Tools\Search%20Paths=auxfiles
Tools\Log%20Paths=auxfiles
Tools\PDF%20Paths=auxfiles
Tools\Auto%20Checkin%20after%20Save=false
Tools\SVN%20Undo=false
Tools\SVN%20KeywordSubstitution=false
Tools\SVN%20Search%20Path%20Depth=2
GUI\Style=false
GUI\Texmaker%20Palette=false
GUI\Use%20System%20Theme=true
X11\Font%20Family=DejaVu Sans
X11\Font%20Size=10
X11\Style=GTK+
GUI\ToobarIconSize=22
GUI\SymbolSize=32
GUI\SecondaryToobarIconSize=16
Interface\Config%20Show%20Advanced%20Options=true
Interface\Config%20Riddled=true
Interface\New%20Left%20Panel%20Layout=true
Interface\MRU%20Document%20Chooser=false
Interface\Language=
Preview\Mode=4
Preview\Auto%20Preview=1
Preview\Auto%20Preview%20Delay=300
Geometries\PdfViewerLeft=1304
Geometries\PdfViewerTop=26
Geometries\PdfViewerWidth=597
Geometries\PdfViewerHeight=1148
Geometries\PdfViewerMaximized=false
Geometries\PdfViewerState=@ByteArray(\0\0\0\xff\0\0\0\0\xfd\0\0\0\x2\0\0\0\0\0\0\0\x92\0\0\x4\x32\xfc\x2\0\0\0\x3\xfb\0\0\0\xe\0o\0u\0t\0l\0i\0n\0\x65\0\0\0\0(\0\0\x4\x32\0\0\0g\0\xff\xff\xff\xfb\0\0\0\xe\0p\0\x64\0\x66\0i\0n\0\x66\0o\0\0\0\0\0\xff\xff\xff\xff\0\0\0g\0\xff\xff\xff\xfb\0\0\0\x10\0o\0v\0\x65\0r\0v\0i\0\x65\0w\0\0\0\0\0\xff\xff\xff\xff\0\0\0g\0\xff\xff\xff\0\0\0\x3\0\0\x3\x9f\0\0\0\x96\xfc\x1\0\0\0\x3\xfb\0\0\0\f\0s\0\x65\0\x61\0r\0\x63\0h\x2\0\0\x1\"\0\0\x2\xec\0\0\x3\xa3\0\0\0\x98\xfb\0\0\0\n\0\x66\0o\0n\0t\0s\0\0\0\0\0\0\0\x3\x9f\0\0\0O\0\xff\xff\xff\xfb\0\0\0\n\0\x63\0l\0o\0\x63\0k\0\0\0\0\0\xff\xff\xff\xff\0\0\0O\0\xff\xff\xff\0\0\x3\x9f\0\0\x4\x32\0\0\0\x4\0\0\0\x4\0\0\0\b\0\0\0\b\xfc\0\0\0\x1\0\0\0\x2\0\0\0\x1\0\0\0\xe\0t\0o\0o\0l\0\x42\0\x61\0r\x1\0\0\0\0\xff\xff\xff\xff\0\0\0\0\0\0\0\0)
Preview\CacheSize=512
Preview\LoadStrategy=2
Preview\DPI=96
Preview\Scale%20Option=1
Preview\Scale=110
Preview\ZoomStepFactor=1.4142135
Preview\Magnifier%20Size=300
Preview\Magnifier%20Shape=1
Preview\Magnifier%20Border=false
Preview\HighlightColor=#FFFF003F
Preview\HighlightDuration=2000
Preview\Sync%20File%20Mask=*.tex
Preview\AutoHideToolbars=false
Preview\EnlargedEmbedded=false
LogView\WarnIfFileSizeLargerMB=2
Tools\User%20Class=@Invalid()
Tools\User%20Paper=@Invalid()
Tools\User%20Encoding=@Invalid()
Tools\User%20Options=@Invalid()
Quick\Class=article
Quick\Typeface=10pt
Quick\Papersize=a4paper
Quick\Encoding=utf8
Quick\AMS=true
Quick\MakeIndex=false
Quick\graphicx=false
Quick\Author=
Quick\Geometry%20Page%20Width=0
Quick\Geometry%20Page%20Height=0
Quick\Geometry%20Margin%20Left=0
Quick\Geometry%20Margin%20Right=0
Quick\Geometry%20Margin%20Top=0
Quick\Geometry%20Margin%20Bottom=0
Quick\Geometry%20Page%20Width%20Unit=cm
Quick\Geometry%20Page%20Height%20Unit=cm
Quick\Geometry%20Margin%20Left%20Unit=cm
Quick\Geometry%20Margin%20Right%20Unit=cm
Quick\Geometry%20Margin%20Top%20Unit=cm
Quick\Geometry%20Margin%20Bottom%20Unit=cm
Quick\Geometry%20Page%20Width%20Enabled=false
Quick\Geometry%20Page%20Height%20Enabled=false
Quick\Geometry%20Margin%20Left%20Enabled=false
Quick\Geometry%20Margin%20Right%20Enabled=false
Quick\Geometry%20Margin%20Top%20Enabled=false
Quick\Geometry%20Margin%20Bottom%20Enabled=false
Beamer\Encoding=utf8
Tools\Quick%20Mode=6
Tools\Max%20Expanding%20Nesting%20Deep=10
Tools\Dvi2Png%20Mode=0
Files\Save%20Files%20Before%20Compiling=2
Preview\Remove%20Beamer%20Class=true
Preview\Precompile%20Preamble=true
Tools\Automatic%20Rerun%20Commands=latex|pdflatex|xelatex|lualatex|compile
User\ToolNames=@Invalid()
User\Tools=@Invalid()
Tools\Display%20Names=slide chain, , makeglossary
Tools\User%20Order=userold1, user0, makeglossary
Tools\Preview%20Compile%20Time%20Out=15000
Tools\Had%20Successful%20Process%20Start=true
Files\Default%20File%20Filter=TeX files (*.tex *.bib *.sty *.cls *.mp *.dtx *.cfg *.ins *.ltx)
PDFSplitter=0.751048951048951
Tools\Kind\LaTeX=latex, pdflatex, xelatex, lualatex, latexmk, compile
Tools\Kind\Rerunnable=latex, pdflatex, xelatex, lualatex
Tools\Kind\Pdf=pdflatex, xelatex, lualatex, latexmk, dvipdf, ps2pdf
Tools\Kind\Stdout=bibtex, biber, bibtex8, bibliography
Tools\Kind\Viewer=view-pdf, view-ps, view-dvi, view-pdf-internal, view-pdf-external, view
Crash%20Handler%20Type=1
Grammar\Display%20Error%200=true
Grammar\Display%20Error%201=true
Grammar\Display%20Error%202=true
Grammar\Display%20Error%203=true
Grammar\Display%20Error%204=true
Grammar\Display%20Error%205=true
Grammar\Display%20Error%206=true
Grammar\Display%20Error%207=true
Search\Find%20History=V, V-, inline, praca-img/, that, learning, equation, agent, wrap, ([^\\s]+) [^\\n]*, ([^\\s]+) [^\\n]*\\n, \\([0-9]+\\.[0-9]+\\), bprime, Obr.~, "Obr. ", ruleset, rulese, IF, POTOM, THEN, usepackage, caption, mylisting, code, table, wrapfigure, wrapfig, \\figure, tablena, \\begin{table, \\act, acro, "\\$[^\\$]+,[^$]\\$", "\\$[^\\$]+[0-9]+,[0-9]+[^$]\\$", \\$[^\\$]+[0-9]+\\.[0-9]+[^$]\\$, \\num, "[0-9]+,[0-9]+", HBox, cluste, seq:refText179, columnsep, wrapfi, seq:refObr33, seq:refObr34, seq:refObr35, seq:refObr36, SAR, \\caption[v], [A-Z], [A-Z][A-Z]+, fuzzy in, addtr, evolutiona, tab, FoM, seq:refText183}, pdfparse, StudentSticky, inputcode, task_comple, char_funkcia, POMDP, task, Markov, skryt, Multi-greedy Policies, radi\xe1l, GPI, section, Monte C, bellm, bellman_2, "[a-z]{1,3}", " [a-z]{1,3}[ \\.]", " [a-z]{1,3}", " [a-z]{,3}", "[a-z]{,3}", tabular, multi, politi, ~, predch\xe1\x64zaj\xfa\x63\x61, (, roztia, Troju, mo\x17eno, samo, samoor, samoo, todo, Zov\x161\x65ob, Kompe, MLP, note, GPGPU, sec:multi-greedy, multi-greedy, angl, seq:refObr1}, angl., polit, polit\xedk, politik, calc, makeat, @@, Klasifi, column, sign, Rprop, row, header, Defin, algorith, dropo, min, signifi, hebbian, spsp, sp, Docum, optional, attachfile, neexis, gradie, pomocou fu, verif, algori, example, inak, text{, experimen, discre, twosi, keyen, keys, slovak, language, label{[^}]*, \\\\label[^}]*-, ex:uc-tasks, ex:multi-greedy, mathit, Love2008, ss', else, mathrm, textrm, ss, \\mathit{, padorabots, thanks, arab, chatbot-*, chatbot-, appendix, appe, defau, autore, goal, sk_, sk_tables, "One of the ", include, includeg, inverse, anfis, winner-take, {algorith, Scala, algorithm, obr\xe1, lst:gd_linearna_regresia, \\listingscaption, \\algorithmcfname, algorithmname, jordanova_siet, dopredna_siet, tabula, vstup, po\x17e\x61\x64ovan\xfd v, po\x17e\x61\x64ova, viewonly, option, optimistic in, optimism, "since ", autoen, ann, space, ucebnica, graphi, Presentatio, zov\x161, postup n, ucebni, E(, f_o, 2-r, sam, SOM, listing, captions, ann_regression, cluster, term, inlineco, pagela, obor, begin{verba, end{verba, eq:hopfield_energy, \\label{eq:hopfield_energy, norm\xe1l, vid, _vid, O, Log, benchma, " F", genera\x10d, kr\xed\x17e\x65, \\caption, separova, \\math, benchm, Siri, MCMC, BS, Defin\xed\x63i, ESO, fig:eso, hlad, greedy, ImageN, V-le, R_{\\mathit{ss'}}^a, R_{\\mathit{s, R_{ss'}^a, R_{\\mathit{ss}'}^{a}, R_{ss'}^{a}, TD(0, epoc, vrstv, state, vrstve, nevrstv, kontrolo, RL, BSD, pageb, extras, pagebr, aproxi, prech, function, algo, crossover, rozumn, prijate, SAT, logick, break, cite, hyperre, hyper, figure, above, aboves, auxfile, inlinec, attac, zov\x161\x65obecnen\xe1 inter\xe1\x63ia, zovseobecnena_iteracia_strategiami, Iter\xe1, email, makecvtitle, emailsymbol, homepagesymbol, fancy, pagestyle, pr\xedz, value_function_approximation, cit\xe1, ch\xfd\x62\x61 z, nei
Search\Case%20Sensitive=true
Search\Whole%20Words=false
Search\Regular%20Expression=false
Search\Highlight=true
Search\Cursor=true
Search\Selection=false
Search\Replace%20History=, \\inputminted, \\1, \\figurename, RequirePackage, eq:delta_error_output, fig:gradient_funkcia, fig:gradient_funkcia_2d, fig:gradient_funkcia_mini, fig:gradient_funkcia_2d_mini, eq:backpropagation}, fig:local_generalization}, ex:uc_tasks, sec:multi_greedy, ex:multi_greedy, pandorabots, tables, \\listingname, algorithmname, \\algorithmname, begin{Verba, end{Verba, fig:esn, r_{\\mathit{ss}'}^{a}
Search\Ask%20before%20Replace=false
Search\Escape%20Sequence=false
Preview\EditTool=1
Preview\Grid%20Sizes=3x1, 3x2, 3x3
Preview\GridX=1
Preview\GridY=1
Preview\Single%20Page%20Step=true
Preview\Continuous=true
Preview\Scrolling%20Follows%20Cursor=false
Preview\Cursor%20Follows%20Scrolling=false
Preview\Sync%20Multiple%20Views=true
Preview\Invert%20Colors=false
Preview\Grayscale=false
Editor\Completion%20Files=acronym.cwl, amsfonts.cwl, amsmath.cwl, amssymb.cwl, array.cwl, babel.cwl, caption.cwl, cite.cwl, color.cwl, fancybox.cwl, fancyhdr.cwl, fontenc.cwl, geometry.cwl, graphicx.cwl, hyperref.cwl, inputenc.cwl, keycommand.cwl, latex-document.cwl, latex-mathsymbols.cwl, mgheader.cwl, newfloat.cwl, subfig.cwl, subfigure.cwl, tex.cwl, tikz.cwl, titlesec.cwl, wrapfig.cwl, xcolor.cwl
qttwp\userwidth=700
qttwp\compil=1
qttwp\tocdepth=2
qttwp\startindex=1
qttwp\navigation=1
qttwp\noindex=false
qttwp\title=
qttwp\address=
qttwp\browser=firefox
qttwp\contentname=\\contentsname
qttwp\align=center
qttwp\lastdir=/home/michalgregor
qttwp\dviopt=" -Ppk -V"
InsertGraphics\includeOptions="width=0.7\\linewidth"
InsertGraphics\center=true
InsertGraphics\useFigure=true
InsertGraphics\captionBelow=true
InsertGraphics\placement=
InsertGraphics\spanTwoCols=false
Tools\After%20BibTeX%20Change=tmx://latex && tmx://bibtex && tmx://latex
User\New%20Key%20Replacements%20Created=true
Macros\0=Key replacement: % before word, %%, , "(?language:latex)(?<=\\s|^)%"
Macros\1=Key replacement: % after word, " %%", , "(?language:latex)(?<=\\S)%"
Macros\2=Table import, "%SCRIPT\ntext = cursor.selectedText()\ntext = text.replace(/\\t/g, \" & \")\ntext = text.replace(/\\n/g, \" \\\\\\\\\\n\\\\\\hline \")\ntext = text.replace(/^/g, \"\\\\\\hline \")\ntext = text.replace(/$/g, \"\\\\\\\\\\n\\\\\\hline\")\ncursor.insertText(text)", , 
Macros\3=DeclareAbbrev, "%SCRIPT\ntext = cursor.selectedText()\ntext = text.replace(/([^\\t]*)\\t([^\\n]*)(?:\\n|$)/g,\n \"\\\\DeclareAcronym{$1} {\\n\\tshort = $1,\\n\\tlong = $2,\\n\\tclass = abbrev\\n}\\n\\n\")\ncursor.insertText(text)", , 
Macros\4=DeclareTerm, "%SCRIPT\ntext = cursor.selectedText()\ntext = text.replace(/([^\\t]*)\\t([^\\n]*)(?:\\n|$)/g,\n\"\\\\DeclareAcronym{$1} {\\n\\tshort = $1,\\n\\tlong = $2,\\n\\tclass = term\\n}\\n\\n\")\ncursor.insertText(text)", , 
Macros\5=Remove hard breaks, "%SCRIPT\ntext = cursor.selectedText()\ntext = text.replace(/\\n/g,\" \")\ntext = text.replace(/  /g,\" \")\ncursor.insertText(text)", , 
Macros\6=Mark as number, "%SCRIPT\ntext = cursor.selectedText()\ncursor.insertText('\\\\num{' + text + '}')", , 
Macros\7=Add columns, "%SCRIPT\ntext = cursor.selectedText()\ntext = '\\\\begin{columns}\\n\\\\begin{column}{0.5\\\\textwidth}\\n' + text + '\\n\\\\end{column}'\ntext = text + '\\n\\\\begin{column}{0.5\\\\textwidth}\\n\\n\\\\end{column}'\ntext = text + '\\n\\\\end{columns}'\n\ncursor.insertText(text)", , 
Macros\8=Add angl, "%SCRIPT\ntext = cursor.selectedText()\ncursor.insertText('\\\\angl{' + text + '}')", , 
Macros\9=Add texttt, "%SCRIPT\ntext = cursor.selectedText()\ncursor.insertText('\\\\texttt{' + text + '}')", , 
keysetting\1\id=main/edit/searching/findinsamedir~1
keysetting\1\key=
keysetting\2\id=main/edit/goto/line~0
keysetting\2\key=
keysetting\3\id=main/edit2/comment~0
keysetting\3\key=Ctrl+D
keysetting\4\id=main/edit2/uncomment~0
keysetting\4\key=Ctrl+Shift+D
keysetting\5\id=main/tools/user/cmd0~0
keysetting\5\key=F10
keysetting\6\id=main/tools/clean~0
keysetting\6\key=Ctrl+Shift+E
keysetting\7\id=main/tools/spelling~0
keysetting\7\key=Ctrl+:
keysetting\8\id=main/latex/environment/common~0
keysetting\8\key=
keysetting\9\id=main/latex/fontstyles/emph~0
keysetting\9\key=
keysetting\10\id=main/math/subscript~0
keysetting\10\key=
keysetting\11\id=main/macros/tag2~0
keysetting\11\key=
keysetting\12\id=main/macros/tag5~0
keysetting\12\key=
keysetting\13\id=main/macros/tag6~0
keysetting\13\key=Ctrl+G
keysetting\14\id=main/macros/tag7~0
keysetting\14\key=Ctrl+L
keysetting\15\id=main/macros/tag8~0
keysetting\15\key=Ctrl+E
keysetting\16\id=main/macros/tag9~0
keysetting\16\key=Ctrl+T
keysetting\size=16
changedLatexMenus=@Variant(\0\0\0\b\0\0\0\0)
CustomToolBar=main/tools/user/cmd0, main/tools/clean
FileToolBar=@Invalid()
EditToolBar=@Invalid()
ToolsToolBar=@Invalid()
MathToolBar=@Invalid()
FormatToolBar=@Invalid()
TableToolBar=@Invalid()
DiffToolBar=@Invalid()
CentralToolBar=@Invalid()
customIcons=@Variant(\0\0\0\b\0\0\0\x1\0\0\0(\0m\0\x61\0i\0n\0/\0t\0o\0o\0l\0s\0/\0u\0s\0\x65\0r\0/\0\x63\0m\0\x64\0\x30\0\0\0\n\0\0\0z\0/\0h\0o\0m\0\x65\0/\0m\0i\0\x63\0h\0\x61\0l\0g\0r\0\x65\0g\0o\0r\0/\0P\0i\0\x63\0t\0u\0r\0\x65\0s\0/\0P\0i\0\x63\0t\0u\0r\0\x65\0 \0\x41\0r\0\x63\0h\0i\0v\0\x65\0s\0/\0I\0\x63\0o\0n\0s\0/\0l\0i\0n\0g\0\x65\0\x61\0.\0p\0n\0g)
customHighlighting=@Variant(\0\0\0\b\0\0\0\0)
customCommands=@Invalid()
Symbols\Quantity=@Variant(\0\0\0\b\0\0\0\a\0\0\0\x36\0r\0\x65\0l\0\x61\0t\0i\0o\0n\0/\0i\0m\0g\0\x30\0\x37\0\x30\0r\0\x65\0l\0\x61\0t\0i\0o\0n\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0\x36\0r\0\x65\0l\0\x61\0t\0i\0o\0n\0/\0i\0m\0g\0\x30\0\x36\0\x39\0r\0\x65\0l\0\x61\0t\0i\0o\0n\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0:\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0/\0i\0m\0g\0\x30\0\x35\0\x30\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0:\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0/\0i\0m\0g\0\x30\0\x34\0\x30\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0:\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0/\0i\0m\0g\0\x30\0\x33\0\x39\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0:\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0/\0i\0m\0g\0\x30\0\x30\0\x35\0o\0p\0\x65\0r\0\x61\0t\0o\0r\0s\0.\0s\0v\0g\0\0\0\x2\0\0\0\x1\0\0\0.\0\x61\0r\0r\0o\0w\0s\0/\0i\0m\0g\0\x30\0\x33\0\x30\0\x61\0r\0r\0o\0w\0s\0.\0s\0v\0g\0\0\0\x2\0\0\0\x2)
Symbols\Favorite%20IDs=@Invalid()
Symbols\hiddenlists=
StructureView\SingleDocMode=false

[formats]
version=1.0
